//1
const strings = ["travel", "hello", "eat", "ski", "lift"];

function countLongStrings(arr) {
    const longStrings = arr.filter(str => str.length > 3);
    return longStrings.length;
}

const count = countLongStrings(strings);
console.log("Кількість рядків з довжиною більше за 3 символи:", count);

//2
const people = [
   { name: "Іван", age: 25, sex: "чоловіча" },
   { name: "Марія", age: 22, sex: "жіноча" },
   { name: "Петро", age: 30, sex: "чоловіча" },
   { name: "Олена", age: 28, sex: "жіноча" }
];
const males = people.filter(person => person.sex === "чоловіча");

console.log("Чоловіки в масиві:", males);

//3
function filterBy(arr, dataType) {
   return arr.filter(item => typeof item !== dataType);
}

const mixedArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(mixedArray, 'string');

console.log("Масив після фільтрації:", filteredArray);
